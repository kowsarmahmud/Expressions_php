
<?php

var_dump(0 == "a"); // 0 == 0 -> true
echo '<br>';
var_dump("1" == "01"); // 1 == 1 -> true
echo '<br>';
var_dump("10" == "1e1"); // 10 == 10 -> true
echo '<br>';
var_dump(100 == "1e2"); // 100 == 100 -> true
echo '<br>';

switch ("a") {
case 0:
    echo "0";
    break;
case "a": // never reached because "a" is already matched with 0
    echo "a";
    break;
}